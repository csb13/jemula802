
cd Algo6_Algo6
thrp1=`tail -n 2 thrp_SA2_DA1_AC1_ID_30_saturation__End.m  | grep 100000.0 | awk '{print $6}'`
thrp2=`tail -n 2 thrp_SA3_DA1_AC1_ID_44_saturation__End.m  | grep 100000.0 | awk '{print $6}'`
thrp3=`tail -n 2 thrp_SA4_DA1_AC1_ID_58_saturation__End.m  | grep 100000.0 | awk '{print $6}'`
thrp4=`tail -n 2 thrp_SA5_DA1_AC1_ID_72_saturation__End.m  | grep 100000.0 | awk '{print $6}'`
thrp5=`tail -n 2 thrp_SA6_DA1_AC1_ID_86_saturation__End.m  | grep 100000.0 | awk '{print $6}'`
thrp6=`tail -n 2 thrp_SA7_DA1_AC1_ID_100_saturation__End.m | grep 100000.0 | awk '{print $6}'`
echo Algo6, Algo6, $thrp1, $thrp2, $thrp3, $thrp4, $thrp5, $thrp6
cd ..

cd Algo6_BlueSky
thrp1=`tail -n 2 thrp_SA2_DA1_AC1_ID_30_saturation__End.m  | grep 100000.0 | awk '{print $6}'`
thrp2=`tail -n 2 thrp_SA3_DA1_AC1_ID_44_saturation__End.m  | grep 100000.0 | awk '{print $6}'`
thrp3=`tail -n 2 thrp_SA4_DA1_AC1_ID_58_saturation__End.m  | grep 100000.0 | awk '{print $6}'`
thrp4=`tail -n 2 thrp_SA5_DA1_AC1_ID_72_saturation__End.m  | grep 100000.0 | awk '{print $6}'`
thrp5=`tail -n 2 thrp_SA6_DA1_AC1_ID_86_saturation__End.m  | grep 100000.0 | awk '{print $6}'`
thrp6=`tail -n 2 thrp_SA7_DA1_AC1_ID_100_saturation__End.m | grep 100000.0 | awk '{print $6}'`
echo Algo6, BlueSky, $thrp1, $thrp2, $thrp3, $thrp4, $thrp5, $thrp6
cd ..

cd Algo6_BroFi
thrp1=`tail -n 2 thrp_SA2_DA1_AC1_ID_30_saturation__End.m  | grep 100000.0 | awk '{print $6}'`
thrp2=`tail -n 2 thrp_SA3_DA1_AC1_ID_44_saturation__End.m  | grep 100000.0 | awk '{print $6}'`
thrp3=`tail -n 2 thrp_SA4_DA1_AC1_ID_58_saturation__End.m  | grep 100000.0 | awk '{print $6}'`
thrp4=`tail -n 2 thrp_SA5_DA1_AC1_ID_72_saturation__End.m  | grep 100000.0 | awk '{print $6}'`
thrp5=`tail -n 2 thrp_SA6_DA1_AC1_ID_86_saturation__End.m  | grep 100000.0 | awk '{print $6}'`
thrp6=`tail -n 2 thrp_SA7_DA1_AC1_ID_100_saturation__End.m | grep 100000.0 | awk '{print $6}'`
echo Algo6, BroFi, $thrp1, $thrp2, $thrp3, $thrp4, $thrp5, $thrp6
cd ..

cd Algo6_Classic2
thrp1=`tail -n 2 thrp_SA2_DA1_AC1_ID_30_saturation__End.m  | grep 100000.0 | awk '{print $6}'`
thrp2=`tail -n 2 thrp_SA3_DA1_AC1_ID_44_saturation__End.m  | grep 100000.0 | awk '{print $6}'`
thrp3=`tail -n 2 thrp_SA4_DA1_AC1_ID_58_saturation__End.m  | grep 100000.0 | awk '{print $6}'`
thrp4=`tail -n 2 thrp_SA5_DA1_AC1_ID_72_saturation__End.m  | grep 100000.0 | awk '{print $6}'`
thrp5=`tail -n 2 thrp_SA6_DA1_AC1_ID_86_saturation__End.m  | grep 100000.0 | awk '{print $6}'`
thrp6=`tail -n 2 thrp_SA7_DA1_AC1_ID_100_saturation__End.m | grep 100000.0 | awk '{print $6}'`
echo Algo6, Classic2, $thrp1, $thrp2, $thrp3, $thrp4, $thrp5, $thrp6
cd ..

cd Algo6_FairShare
thrp1=`tail -n 2 thrp_SA2_DA1_AC1_ID_30_saturation__End.m  | grep 100000.0 | awk '{print $6}'`
thrp2=`tail -n 2 thrp_SA3_DA1_AC1_ID_44_saturation__End.m  | grep 100000.0 | awk '{print $6}'`
thrp3=`tail -n 2 thrp_SA4_DA1_AC1_ID_58_saturation__End.m  | grep 100000.0 | awk '{print $6}'`
thrp4=`tail -n 2 thrp_SA5_DA1_AC1_ID_72_saturation__End.m  | grep 100000.0 | awk '{print $6}'`
thrp5=`tail -n 2 thrp_SA6_DA1_AC1_ID_86_saturation__End.m  | grep 100000.0 | awk '{print $6}'`
thrp6=`tail -n 2 thrp_SA7_DA1_AC1_ID_100_saturation__End.m | grep 100000.0 | awk '{print $6}'`
echo Algo6, FairShare, $thrp1, $thrp2, $thrp3, $thrp4, $thrp5, $thrp6
cd ..

cd Algo6_FindingNash
thrp1=`tail -n 2 thrp_SA2_DA1_AC1_ID_30_saturation__End.m  | grep 100000.0 | awk '{print $6}'`
thrp2=`tail -n 2 thrp_SA3_DA1_AC1_ID_44_saturation__End.m  | grep 100000.0 | awk '{print $6}'`
thrp3=`tail -n 2 thrp_SA4_DA1_AC1_ID_58_saturation__End.m  | grep 100000.0 | awk '{print $6}'`
thrp4=`tail -n 2 thrp_SA5_DA1_AC1_ID_72_saturation__End.m  | grep 100000.0 | awk '{print $6}'`
thrp5=`tail -n 2 thrp_SA6_DA1_AC1_ID_86_saturation__End.m  | grep 100000.0 | awk '{print $6}'`
thrp6=`tail -n 2 thrp_SA7_DA1_AC1_ID_100_saturation__End.m | grep 100000.0 | awk '{print $6}'`
echo Algo6, FindingNash, $thrp1, $thrp2, $thrp3, $thrp4, $thrp5, $thrp6
cd ..

cd Algo6_Freeze
thrp1=`tail -n 2 thrp_SA2_DA1_AC1_ID_30_saturation__End.m  | grep 100000.0 | awk '{print $6}'`
thrp2=`tail -n 2 thrp_SA3_DA1_AC1_ID_44_saturation__End.m  | grep 100000.0 | awk '{print $6}'`
thrp3=`tail -n 2 thrp_SA4_DA1_AC1_ID_58_saturation__End.m  | grep 100000.0 | awk '{print $6}'`
thrp4=`tail -n 2 thrp_SA5_DA1_AC1_ID_72_saturation__End.m  | grep 100000.0 | awk '{print $6}'`
thrp5=`tail -n 2 thrp_SA6_DA1_AC1_ID_86_saturation__End.m  | grep 100000.0 | awk '{print $6}'`
thrp6=`tail -n 2 thrp_SA7_DA1_AC1_ID_100_saturation__End.m | grep 100000.0 | awk '{print $6}'`
echo Algo6, Freeze, $thrp1, $thrp2, $thrp3, $thrp4, $thrp5, $thrp6
cd ..

cd Algo6_None
thrp1=`tail -n 2 thrp_SA2_DA1_AC1_ID_30_saturation__End.m  | grep 100000.0 | awk '{print $6}'`
thrp2=`tail -n 2 thrp_SA3_DA1_AC1_ID_44_saturation__End.m  | grep 100000.0 | awk '{print $6}'`
thrp3=`tail -n 2 thrp_SA4_DA1_AC1_ID_58_saturation__End.m  | grep 100000.0 | awk '{print $6}'`
thrp4=`tail -n 2 thrp_SA5_DA1_AC1_ID_72_saturation__End.m  | grep 100000.0 | awk '{print $6}'`
thrp5=`tail -n 2 thrp_SA6_DA1_AC1_ID_86_saturation__End.m  | grep 100000.0 | awk '{print $6}'`
thrp6=`tail -n 2 thrp_SA7_DA1_AC1_ID_100_saturation__End.m | grep 100000.0 | awk '{print $6}'`
echo Algo6, None, $thrp1, $thrp2, $thrp3, $thrp4, $thrp5, $thrp6
cd ..

cd Algo6_PFA
thrp1=`tail -n 2 thrp_SA2_DA1_AC1_ID_30_saturation__End.m  | grep 100000.0 | awk '{print $6}'`
thrp2=`tail -n 2 thrp_SA3_DA1_AC1_ID_44_saturation__End.m  | grep 100000.0 | awk '{print $6}'`
thrp3=`tail -n 2 thrp_SA4_DA1_AC1_ID_58_saturation__End.m  | grep 100000.0 | awk '{print $6}'`
thrp4=`tail -n 2 thrp_SA5_DA1_AC1_ID_72_saturation__End.m  | grep 100000.0 | awk '{print $6}'`
thrp5=`tail -n 2 thrp_SA6_DA1_AC1_ID_86_saturation__End.m  | grep 100000.0 | awk '{print $6}'`
thrp6=`tail -n 2 thrp_SA7_DA1_AC1_ID_100_saturation__End.m | grep 100000.0 | awk '{print $6}'`
echo Algo6, PFA, $thrp1, $thrp2, $thrp3, $thrp4, $thrp5, $thrp6
cd ..

cd Algo6_Sneaky
thrp1=`tail -n 2 thrp_SA2_DA1_AC1_ID_30_saturation__End.m  | grep 100000.0 | awk '{print $6}'`
thrp2=`tail -n 2 thrp_SA3_DA1_AC1_ID_44_saturation__End.m  | grep 100000.0 | awk '{print $6}'`
thrp3=`tail -n 2 thrp_SA4_DA1_AC1_ID_58_saturation__End.m  | grep 100000.0 | awk '{print $6}'`
thrp4=`tail -n 2 thrp_SA5_DA1_AC1_ID_72_saturation__End.m  | grep 100000.0 | awk '{print $6}'`
thrp5=`tail -n 2 thrp_SA6_DA1_AC1_ID_86_saturation__End.m  | grep 100000.0 | awk '{print $6}'`
thrp6=`tail -n 2 thrp_SA7_DA1_AC1_ID_100_saturation__End.m | grep 100000.0 | awk '{print $6}'`
echo Algo6, Sneaky, $thrp1, $thrp2, $thrp3, $thrp4, $thrp5, $thrp6
cd ..

cd Algo6_StarFighter
thrp1=`tail -n 2 thrp_SA2_DA1_AC1_ID_30_saturation__End.m  | grep 100000.0 | awk '{print $6}'`
thrp2=`tail -n 2 thrp_SA3_DA1_AC1_ID_44_saturation__End.m  | grep 100000.0 | awk '{print $6}'`
thrp3=`tail -n 2 thrp_SA4_DA1_AC1_ID_58_saturation__End.m  | grep 100000.0 | awk '{print $6}'`
thrp4=`tail -n 2 thrp_SA5_DA1_AC1_ID_72_saturation__End.m  | grep 100000.0 | awk '{print $6}'`
thrp5=`tail -n 2 thrp_SA6_DA1_AC1_ID_86_saturation__End.m  | grep 100000.0 | awk '{print $6}'`
thrp6=`tail -n 2 thrp_SA7_DA1_AC1_ID_100_saturation__End.m | grep 100000.0 | awk '{print $6}'`
echo Algo6, StarFighter, $thrp1, $thrp2, $thrp3, $thrp4, $thrp5, $thrp6
cd ..

cd Algo6_Vincent
thrp1=`tail -n 2 thrp_SA2_DA1_AC1_ID_30_saturation__End.m  | grep 100000.0 | awk '{print $6}'`
thrp2=`tail -n 2 thrp_SA3_DA1_AC1_ID_44_saturation__End.m  | grep 100000.0 | awk '{print $6}'`
thrp3=`tail -n 2 thrp_SA4_DA1_AC1_ID_58_saturation__End.m  | grep 100000.0 | awk '{print $6}'`
thrp4=`tail -n 2 thrp_SA5_DA1_AC1_ID_72_saturation__End.m  | grep 100000.0 | awk '{print $6}'`
thrp5=`tail -n 2 thrp_SA6_DA1_AC1_ID_86_saturation__End.m  | grep 100000.0 | awk '{print $6}'`
thrp6=`tail -n 2 thrp_SA7_DA1_AC1_ID_100_saturation__End.m | grep 100000.0 | awk '{print $6}'`
echo Algo6, Vincent, $thrp1, $thrp2, $thrp3, $thrp4, $thrp5, $thrp6
cd ..

cd Algo6_Yacmoca
thrp1=`tail -n 2 thrp_SA2_DA1_AC1_ID_30_saturation__End.m  | grep 100000.0 | awk '{print $6}'`
thrp2=`tail -n 2 thrp_SA3_DA1_AC1_ID_44_saturation__End.m  | grep 100000.0 | awk '{print $6}'`
thrp3=`tail -n 2 thrp_SA4_DA1_AC1_ID_58_saturation__End.m  | grep 100000.0 | awk '{print $6}'`
thrp4=`tail -n 2 thrp_SA5_DA1_AC1_ID_72_saturation__End.m  | grep 100000.0 | awk '{print $6}'`
thrp5=`tail -n 2 thrp_SA6_DA1_AC1_ID_86_saturation__End.m  | grep 100000.0 | awk '{print $6}'`
thrp6=`tail -n 2 thrp_SA7_DA1_AC1_ID_100_saturation__End.m | grep 100000.0 | awk '{print $6}'`
echo Algo6, Yacmoca, $thrp1, $thrp2, $thrp3, $thrp4, $thrp5, $thrp6
cd ..

cd BlueSky_BlueSky
thrp1=`tail -n 2 thrp_SA2_DA1_AC1_ID_30_saturation__End.m  | grep 100000.0 | awk '{print $6}'`
thrp2=`tail -n 2 thrp_SA3_DA1_AC1_ID_44_saturation__End.m  | grep 100000.0 | awk '{print $6}'`
thrp3=`tail -n 2 thrp_SA4_DA1_AC1_ID_58_saturation__End.m  | grep 100000.0 | awk '{print $6}'`
thrp4=`tail -n 2 thrp_SA5_DA1_AC1_ID_72_saturation__End.m  | grep 100000.0 | awk '{print $6}'`
thrp5=`tail -n 2 thrp_SA6_DA1_AC1_ID_86_saturation__End.m  | grep 100000.0 | awk '{print $6}'`
thrp6=`tail -n 2 thrp_SA7_DA1_AC1_ID_100_saturation__End.m | grep 100000.0 | awk '{print $6}'`
echo BlueSky, BlueSky, $thrp1, $thrp2, $thrp3, $thrp4, $thrp5, $thrp6
cd ..

cd BlueSky_BroFi
thrp1=`tail -n 2 thrp_SA2_DA1_AC1_ID_30_saturation__End.m  | grep 100000.0 | awk '{print $6}'`
thrp2=`tail -n 2 thrp_SA3_DA1_AC1_ID_44_saturation__End.m  | grep 100000.0 | awk '{print $6}'`
thrp3=`tail -n 2 thrp_SA4_DA1_AC1_ID_58_saturation__End.m  | grep 100000.0 | awk '{print $6}'`
thrp4=`tail -n 2 thrp_SA5_DA1_AC1_ID_72_saturation__End.m  | grep 100000.0 | awk '{print $6}'`
thrp5=`tail -n 2 thrp_SA6_DA1_AC1_ID_86_saturation__End.m  | grep 100000.0 | awk '{print $6}'`
thrp6=`tail -n 2 thrp_SA7_DA1_AC1_ID_100_saturation__End.m | grep 100000.0 | awk '{print $6}'`
echo BlueSky, BroFi, $thrp1, $thrp2, $thrp3, $thrp4, $thrp5, $thrp6
cd ..

cd BlueSky_Classic2
thrp1=`tail -n 2 thrp_SA2_DA1_AC1_ID_30_saturation__End.m  | grep 100000.0 | awk '{print $6}'`
thrp2=`tail -n 2 thrp_SA3_DA1_AC1_ID_44_saturation__End.m  | grep 100000.0 | awk '{print $6}'`
thrp3=`tail -n 2 thrp_SA4_DA1_AC1_ID_58_saturation__End.m  | grep 100000.0 | awk '{print $6}'`
thrp4=`tail -n 2 thrp_SA5_DA1_AC1_ID_72_saturation__End.m  | grep 100000.0 | awk '{print $6}'`
thrp5=`tail -n 2 thrp_SA6_DA1_AC1_ID_86_saturation__End.m  | grep 100000.0 | awk '{print $6}'`
thrp6=`tail -n 2 thrp_SA7_DA1_AC1_ID_100_saturation__End.m | grep 100000.0 | awk '{print $6}'`
echo BlueSky, Classic2, $thrp1, $thrp2, $thrp3, $thrp4, $thrp5, $thrp6
cd ..

cd BlueSky_FairShare
thrp1=`tail -n 2 thrp_SA2_DA1_AC1_ID_30_saturation__End.m  | grep 100000.0 | awk '{print $6}'`
thrp2=`tail -n 2 thrp_SA3_DA1_AC1_ID_44_saturation__End.m  | grep 100000.0 | awk '{print $6}'`
thrp3=`tail -n 2 thrp_SA4_DA1_AC1_ID_58_saturation__End.m  | grep 100000.0 | awk '{print $6}'`
thrp4=`tail -n 2 thrp_SA5_DA1_AC1_ID_72_saturation__End.m  | grep 100000.0 | awk '{print $6}'`
thrp5=`tail -n 2 thrp_SA6_DA1_AC1_ID_86_saturation__End.m  | grep 100000.0 | awk '{print $6}'`
thrp6=`tail -n 2 thrp_SA7_DA1_AC1_ID_100_saturation__End.m | grep 100000.0 | awk '{print $6}'`
echo BlueSky, FairShare, $thrp1, $thrp2, $thrp3, $thrp4, $thrp5, $thrp6
cd ..

cd BlueSky_FindingNash
thrp1=`tail -n 2 thrp_SA2_DA1_AC1_ID_30_saturation__End.m  | grep 100000.0 | awk '{print $6}'`
thrp2=`tail -n 2 thrp_SA3_DA1_AC1_ID_44_saturation__End.m  | grep 100000.0 | awk '{print $6}'`
thrp3=`tail -n 2 thrp_SA4_DA1_AC1_ID_58_saturation__End.m  | grep 100000.0 | awk '{print $6}'`
thrp4=`tail -n 2 thrp_SA5_DA1_AC1_ID_72_saturation__End.m  | grep 100000.0 | awk '{print $6}'`
thrp5=`tail -n 2 thrp_SA6_DA1_AC1_ID_86_saturation__End.m  | grep 100000.0 | awk '{print $6}'`
thrp6=`tail -n 2 thrp_SA7_DA1_AC1_ID_100_saturation__End.m | grep 100000.0 | awk '{print $6}'`
echo BlueSky, FindingNash, $thrp1, $thrp2, $thrp3, $thrp4, $thrp5, $thrp6
cd ..

cd BlueSky_Freeze
thrp1=`tail -n 2 thrp_SA2_DA1_AC1_ID_30_saturation__End.m  | grep 100000.0 | awk '{print $6}'`
thrp2=`tail -n 2 thrp_SA3_DA1_AC1_ID_44_saturation__End.m  | grep 100000.0 | awk '{print $6}'`
thrp3=`tail -n 2 thrp_SA4_DA1_AC1_ID_58_saturation__End.m  | grep 100000.0 | awk '{print $6}'`
thrp4=`tail -n 2 thrp_SA5_DA1_AC1_ID_72_saturation__End.m  | grep 100000.0 | awk '{print $6}'`
thrp5=`tail -n 2 thrp_SA6_DA1_AC1_ID_86_saturation__End.m  | grep 100000.0 | awk '{print $6}'`
thrp6=`tail -n 2 thrp_SA7_DA1_AC1_ID_100_saturation__End.m | grep 100000.0 | awk '{print $6}'`
echo BlueSky, Freeze, $thrp1, $thrp2, $thrp3, $thrp4, $thrp5, $thrp6
cd ..

cd BlueSky_None
thrp1=`tail -n 2 thrp_SA2_DA1_AC1_ID_30_saturation__End.m  | grep 100000.0 | awk '{print $6}'`
thrp2=`tail -n 2 thrp_SA3_DA1_AC1_ID_44_saturation__End.m  | grep 100000.0 | awk '{print $6}'`
thrp3=`tail -n 2 thrp_SA4_DA1_AC1_ID_58_saturation__End.m  | grep 100000.0 | awk '{print $6}'`
thrp4=`tail -n 2 thrp_SA5_DA1_AC1_ID_72_saturation__End.m  | grep 100000.0 | awk '{print $6}'`
thrp5=`tail -n 2 thrp_SA6_DA1_AC1_ID_86_saturation__End.m  | grep 100000.0 | awk '{print $6}'`
thrp6=`tail -n 2 thrp_SA7_DA1_AC1_ID_100_saturation__End.m | grep 100000.0 | awk '{print $6}'`
echo BlueSky, None, $thrp1, $thrp2, $thrp3, $thrp4, $thrp5, $thrp6
cd ..

cd BlueSky_PFA
thrp1=`tail -n 2 thrp_SA2_DA1_AC1_ID_30_saturation__End.m  | grep 100000.0 | awk '{print $6}'`
thrp2=`tail -n 2 thrp_SA3_DA1_AC1_ID_44_saturation__End.m  | grep 100000.0 | awk '{print $6}'`
thrp3=`tail -n 2 thrp_SA4_DA1_AC1_ID_58_saturation__End.m  | grep 100000.0 | awk '{print $6}'`
thrp4=`tail -n 2 thrp_SA5_DA1_AC1_ID_72_saturation__End.m  | grep 100000.0 | awk '{print $6}'`
thrp5=`tail -n 2 thrp_SA6_DA1_AC1_ID_86_saturation__End.m  | grep 100000.0 | awk '{print $6}'`
thrp6=`tail -n 2 thrp_SA7_DA1_AC1_ID_100_saturation__End.m | grep 100000.0 | awk '{print $6}'`
echo BlueSky, PFA, $thrp1, $thrp2, $thrp3, $thrp4, $thrp5, $thrp6
cd ..

cd BlueSky_Sneaky
thrp1=`tail -n 2 thrp_SA2_DA1_AC1_ID_30_saturation__End.m  | grep 100000.0 | awk '{print $6}'`
thrp2=`tail -n 2 thrp_SA3_DA1_AC1_ID_44_saturation__End.m  | grep 100000.0 | awk '{print $6}'`
thrp3=`tail -n 2 thrp_SA4_DA1_AC1_ID_58_saturation__End.m  | grep 100000.0 | awk '{print $6}'`
thrp4=`tail -n 2 thrp_SA5_DA1_AC1_ID_72_saturation__End.m  | grep 100000.0 | awk '{print $6}'`
thrp5=`tail -n 2 thrp_SA6_DA1_AC1_ID_86_saturation__End.m  | grep 100000.0 | awk '{print $6}'`
thrp6=`tail -n 2 thrp_SA7_DA1_AC1_ID_100_saturation__End.m | grep 100000.0 | awk '{print $6}'`
echo BlueSky, Sneaky, $thrp1, $thrp2, $thrp3, $thrp4, $thrp5, $thrp6
cd ..

cd BlueSky_StarFighter
thrp1=`tail -n 2 thrp_SA2_DA1_AC1_ID_30_saturation__End.m  | grep 100000.0 | awk '{print $6}'`
thrp2=`tail -n 2 thrp_SA3_DA1_AC1_ID_44_saturation__End.m  | grep 100000.0 | awk '{print $6}'`
thrp3=`tail -n 2 thrp_SA4_DA1_AC1_ID_58_saturation__End.m  | grep 100000.0 | awk '{print $6}'`
thrp4=`tail -n 2 thrp_SA5_DA1_AC1_ID_72_saturation__End.m  | grep 100000.0 | awk '{print $6}'`
thrp5=`tail -n 2 thrp_SA6_DA1_AC1_ID_86_saturation__End.m  | grep 100000.0 | awk '{print $6}'`
thrp6=`tail -n 2 thrp_SA7_DA1_AC1_ID_100_saturation__End.m | grep 100000.0 | awk '{print $6}'`
echo BlueSky, StarFighter, $thrp1, $thrp2, $thrp3, $thrp4, $thrp5, $thrp6
cd ..

cd BlueSky_Vincent
thrp1=`tail -n 2 thrp_SA2_DA1_AC1_ID_30_saturation__End.m  | grep 100000.0 | awk '{print $6}'`
thrp2=`tail -n 2 thrp_SA3_DA1_AC1_ID_44_saturation__End.m  | grep 100000.0 | awk '{print $6}'`
thrp3=`tail -n 2 thrp_SA4_DA1_AC1_ID_58_saturation__End.m  | grep 100000.0 | awk '{print $6}'`
thrp4=`tail -n 2 thrp_SA5_DA1_AC1_ID_72_saturation__End.m  | grep 100000.0 | awk '{print $6}'`
thrp5=`tail -n 2 thrp_SA6_DA1_AC1_ID_86_saturation__End.m  | grep 100000.0 | awk '{print $6}'`
thrp6=`tail -n 2 thrp_SA7_DA1_AC1_ID_100_saturation__End.m | grep 100000.0 | awk '{print $6}'`
echo BlueSky, Vincent, $thrp1, $thrp2, $thrp3, $thrp4, $thrp5, $thrp6
cd ..

cd BlueSky_Yacmoca
thrp1=`tail -n 2 thrp_SA2_DA1_AC1_ID_30_saturation__End.m  | grep 100000.0 | awk '{print $6}'`
thrp2=`tail -n 2 thrp_SA3_DA1_AC1_ID_44_saturation__End.m  | grep 100000.0 | awk '{print $6}'`
thrp3=`tail -n 2 thrp_SA4_DA1_AC1_ID_58_saturation__End.m  | grep 100000.0 | awk '{print $6}'`
thrp4=`tail -n 2 thrp_SA5_DA1_AC1_ID_72_saturation__End.m  | grep 100000.0 | awk '{print $6}'`
thrp5=`tail -n 2 thrp_SA6_DA1_AC1_ID_86_saturation__End.m  | grep 100000.0 | awk '{print $6}'`
thrp6=`tail -n 2 thrp_SA7_DA1_AC1_ID_100_saturation__End.m | grep 100000.0 | awk '{print $6}'`
echo BlueSky, Yacmoca, $thrp1, $thrp2, $thrp3, $thrp4, $thrp5, $thrp6
cd ..

cd BroFi_BroFi
thrp1=`tail -n 2 thrp_SA2_DA1_AC1_ID_30_saturation__End.m  | grep 100000.0 | awk '{print $6}'`
thrp2=`tail -n 2 thrp_SA3_DA1_AC1_ID_44_saturation__End.m  | grep 100000.0 | awk '{print $6}'`
thrp3=`tail -n 2 thrp_SA4_DA1_AC1_ID_58_saturation__End.m  | grep 100000.0 | awk '{print $6}'`
thrp4=`tail -n 2 thrp_SA5_DA1_AC1_ID_72_saturation__End.m  | grep 100000.0 | awk '{print $6}'`
thrp5=`tail -n 2 thrp_SA6_DA1_AC1_ID_86_saturation__End.m  | grep 100000.0 | awk '{print $6}'`
thrp6=`tail -n 2 thrp_SA7_DA1_AC1_ID_100_saturation__End.m | grep 100000.0 | awk '{print $6}'`
echo BroFi, BroFi, $thrp1, $thrp2, $thrp3, $thrp4, $thrp5, $thrp6
cd ..

cd BroFi_Classic2
thrp1=`tail -n 2 thrp_SA2_DA1_AC1_ID_30_saturation__End.m  | grep 100000.0 | awk '{print $6}'`
thrp2=`tail -n 2 thrp_SA3_DA1_AC1_ID_44_saturation__End.m  | grep 100000.0 | awk '{print $6}'`
thrp3=`tail -n 2 thrp_SA4_DA1_AC1_ID_58_saturation__End.m  | grep 100000.0 | awk '{print $6}'`
thrp4=`tail -n 2 thrp_SA5_DA1_AC1_ID_72_saturation__End.m  | grep 100000.0 | awk '{print $6}'`
thrp5=`tail -n 2 thrp_SA6_DA1_AC1_ID_86_saturation__End.m  | grep 100000.0 | awk '{print $6}'`
thrp6=`tail -n 2 thrp_SA7_DA1_AC1_ID_100_saturation__End.m | grep 100000.0 | awk '{print $6}'`
echo BroFi, Classic2, $thrp1, $thrp2, $thrp3, $thrp4, $thrp5, $thrp6
cd ..

cd BroFi_FairShare
thrp1=`tail -n 2 thrp_SA2_DA1_AC1_ID_30_saturation__End.m  | grep 100000.0 | awk '{print $6}'`
thrp2=`tail -n 2 thrp_SA3_DA1_AC1_ID_44_saturation__End.m  | grep 100000.0 | awk '{print $6}'`
thrp3=`tail -n 2 thrp_SA4_DA1_AC1_ID_58_saturation__End.m  | grep 100000.0 | awk '{print $6}'`
thrp4=`tail -n 2 thrp_SA5_DA1_AC1_ID_72_saturation__End.m  | grep 100000.0 | awk '{print $6}'`
thrp5=`tail -n 2 thrp_SA6_DA1_AC1_ID_86_saturation__End.m  | grep 100000.0 | awk '{print $6}'`
thrp6=`tail -n 2 thrp_SA7_DA1_AC1_ID_100_saturation__End.m | grep 100000.0 | awk '{print $6}'`
echo BroFi, FairShare, $thrp1, $thrp2, $thrp3, $thrp4, $thrp5, $thrp6
cd ..

cd BroFi_FindingNash
thrp1=`tail -n 2 thrp_SA2_DA1_AC1_ID_30_saturation__End.m  | grep 100000.0 | awk '{print $6}'`
thrp2=`tail -n 2 thrp_SA3_DA1_AC1_ID_44_saturation__End.m  | grep 100000.0 | awk '{print $6}'`
thrp3=`tail -n 2 thrp_SA4_DA1_AC1_ID_58_saturation__End.m  | grep 100000.0 | awk '{print $6}'`
thrp4=`tail -n 2 thrp_SA5_DA1_AC1_ID_72_saturation__End.m  | grep 100000.0 | awk '{print $6}'`
thrp5=`tail -n 2 thrp_SA6_DA1_AC1_ID_86_saturation__End.m  | grep 100000.0 | awk '{print $6}'`
thrp6=`tail -n 2 thrp_SA7_DA1_AC1_ID_100_saturation__End.m | grep 100000.0 | awk '{print $6}'`
echo BroFi, FindingNash, $thrp1, $thrp2, $thrp3, $thrp4, $thrp5, $thrp6
cd ..

cd BroFi_Freeze
thrp1=`tail -n 2 thrp_SA2_DA1_AC1_ID_30_saturation__End.m  | grep 100000.0 | awk '{print $6}'`
thrp2=`tail -n 2 thrp_SA3_DA1_AC1_ID_44_saturation__End.m  | grep 100000.0 | awk '{print $6}'`
thrp3=`tail -n 2 thrp_SA4_DA1_AC1_ID_58_saturation__End.m  | grep 100000.0 | awk '{print $6}'`
thrp4=`tail -n 2 thrp_SA5_DA1_AC1_ID_72_saturation__End.m  | grep 100000.0 | awk '{print $6}'`
thrp5=`tail -n 2 thrp_SA6_DA1_AC1_ID_86_saturation__End.m  | grep 100000.0 | awk '{print $6}'`
thrp6=`tail -n 2 thrp_SA7_DA1_AC1_ID_100_saturation__End.m | grep 100000.0 | awk '{print $6}'`
echo BroFi, Freeze, $thrp1, $thrp2, $thrp3, $thrp4, $thrp5, $thrp6
cd ..

cd BroFi_None
thrp1=`tail -n 2 thrp_SA2_DA1_AC1_ID_30_saturation__End.m  | grep 100000.0 | awk '{print $6}'`
thrp2=`tail -n 2 thrp_SA3_DA1_AC1_ID_44_saturation__End.m  | grep 100000.0 | awk '{print $6}'`
thrp3=`tail -n 2 thrp_SA4_DA1_AC1_ID_58_saturation__End.m  | grep 100000.0 | awk '{print $6}'`
thrp4=`tail -n 2 thrp_SA5_DA1_AC1_ID_72_saturation__End.m  | grep 100000.0 | awk '{print $6}'`
thrp5=`tail -n 2 thrp_SA6_DA1_AC1_ID_86_saturation__End.m  | grep 100000.0 | awk '{print $6}'`
thrp6=`tail -n 2 thrp_SA7_DA1_AC1_ID_100_saturation__End.m | grep 100000.0 | awk '{print $6}'`
echo BroFi, None, $thrp1, $thrp2, $thrp3, $thrp4, $thrp5, $thrp6
cd ..

cd BroFi_PFA
thrp1=`tail -n 2 thrp_SA2_DA1_AC1_ID_30_saturation__End.m  | grep 100000.0 | awk '{print $6}'`
thrp2=`tail -n 2 thrp_SA3_DA1_AC1_ID_44_saturation__End.m  | grep 100000.0 | awk '{print $6}'`
thrp3=`tail -n 2 thrp_SA4_DA1_AC1_ID_58_saturation__End.m  | grep 100000.0 | awk '{print $6}'`
thrp4=`tail -n 2 thrp_SA5_DA1_AC1_ID_72_saturation__End.m  | grep 100000.0 | awk '{print $6}'`
thrp5=`tail -n 2 thrp_SA6_DA1_AC1_ID_86_saturation__End.m  | grep 100000.0 | awk '{print $6}'`
thrp6=`tail -n 2 thrp_SA7_DA1_AC1_ID_100_saturation__End.m | grep 100000.0 | awk '{print $6}'`
echo BroFi, PFA, $thrp1, $thrp2, $thrp3, $thrp4, $thrp5, $thrp6
cd ..

cd BroFi_Sneaky
thrp1=`tail -n 2 thrp_SA2_DA1_AC1_ID_30_saturation__End.m  | grep 100000.0 | awk '{print $6}'`
thrp2=`tail -n 2 thrp_SA3_DA1_AC1_ID_44_saturation__End.m  | grep 100000.0 | awk '{print $6}'`
thrp3=`tail -n 2 thrp_SA4_DA1_AC1_ID_58_saturation__End.m  | grep 100000.0 | awk '{print $6}'`
thrp4=`tail -n 2 thrp_SA5_DA1_AC1_ID_72_saturation__End.m  | grep 100000.0 | awk '{print $6}'`
thrp5=`tail -n 2 thrp_SA6_DA1_AC1_ID_86_saturation__End.m  | grep 100000.0 | awk '{print $6}'`
thrp6=`tail -n 2 thrp_SA7_DA1_AC1_ID_100_saturation__End.m | grep 100000.0 | awk '{print $6}'`
echo BroFi, Sneaky, $thrp1, $thrp2, $thrp3, $thrp4, $thrp5, $thrp6
cd ..

cd BroFi_StarFighter
thrp1=`tail -n 2 thrp_SA2_DA1_AC1_ID_30_saturation__End.m  | grep 100000.0 | awk '{print $6}'`
thrp2=`tail -n 2 thrp_SA3_DA1_AC1_ID_44_saturation__End.m  | grep 100000.0 | awk '{print $6}'`
thrp3=`tail -n 2 thrp_SA4_DA1_AC1_ID_58_saturation__End.m  | grep 100000.0 | awk '{print $6}'`
thrp4=`tail -n 2 thrp_SA5_DA1_AC1_ID_72_saturation__End.m  | grep 100000.0 | awk '{print $6}'`
thrp5=`tail -n 2 thrp_SA6_DA1_AC1_ID_86_saturation__End.m  | grep 100000.0 | awk '{print $6}'`
thrp6=`tail -n 2 thrp_SA7_DA1_AC1_ID_100_saturation__End.m | grep 100000.0 | awk '{print $6}'`
echo BroFi, StarFighter, $thrp1, $thrp2, $thrp3, $thrp4, $thrp5, $thrp6
cd ..

cd BroFi_Vincent
thrp1=`tail -n 2 thrp_SA2_DA1_AC1_ID_30_saturation__End.m  | grep 100000.0 | awk '{print $6}'`
thrp2=`tail -n 2 thrp_SA3_DA1_AC1_ID_44_saturation__End.m  | grep 100000.0 | awk '{print $6}'`
thrp3=`tail -n 2 thrp_SA4_DA1_AC1_ID_58_saturation__End.m  | grep 100000.0 | awk '{print $6}'`
thrp4=`tail -n 2 thrp_SA5_DA1_AC1_ID_72_saturation__End.m  | grep 100000.0 | awk '{print $6}'`
thrp5=`tail -n 2 thrp_SA6_DA1_AC1_ID_86_saturation__End.m  | grep 100000.0 | awk '{print $6}'`
thrp6=`tail -n 2 thrp_SA7_DA1_AC1_ID_100_saturation__End.m | grep 100000.0 | awk '{print $6}'`
echo BroFi, Vincent, $thrp1, $thrp2, $thrp3, $thrp4, $thrp5, $thrp6
cd ..

cd BroFi_Yacmoca
thrp1=`tail -n 2 thrp_SA2_DA1_AC1_ID_30_saturation__End.m  | grep 100000.0 | awk '{print $6}'`
thrp2=`tail -n 2 thrp_SA3_DA1_AC1_ID_44_saturation__End.m  | grep 100000.0 | awk '{print $6}'`
thrp3=`tail -n 2 thrp_SA4_DA1_AC1_ID_58_saturation__End.m  | grep 100000.0 | awk '{print $6}'`
thrp4=`tail -n 2 thrp_SA5_DA1_AC1_ID_72_saturation__End.m  | grep 100000.0 | awk '{print $6}'`
thrp5=`tail -n 2 thrp_SA6_DA1_AC1_ID_86_saturation__End.m  | grep 100000.0 | awk '{print $6}'`
thrp6=`tail -n 2 thrp_SA7_DA1_AC1_ID_100_saturation__End.m | grep 100000.0 | awk '{print $6}'`
echo BroFi, Yacmoca, $thrp1, $thrp2, $thrp3, $thrp4, $thrp5, $thrp6
cd ..

cd Classic2_Classic2
thrp1=`tail -n 2 thrp_SA2_DA1_AC1_ID_30_saturation__End.m  | grep 100000.0 | awk '{print $6}'`
thrp2=`tail -n 2 thrp_SA3_DA1_AC1_ID_44_saturation__End.m  | grep 100000.0 | awk '{print $6}'`
thrp3=`tail -n 2 thrp_SA4_DA1_AC1_ID_58_saturation__End.m  | grep 100000.0 | awk '{print $6}'`
thrp4=`tail -n 2 thrp_SA5_DA1_AC1_ID_72_saturation__End.m  | grep 100000.0 | awk '{print $6}'`
thrp5=`tail -n 2 thrp_SA6_DA1_AC1_ID_86_saturation__End.m  | grep 100000.0 | awk '{print $6}'`
thrp6=`tail -n 2 thrp_SA7_DA1_AC1_ID_100_saturation__End.m | grep 100000.0 | awk '{print $6}'`
echo Classic2, Classic2, $thrp1, $thrp2, $thrp3, $thrp4, $thrp5, $thrp6
cd ..

cd Classic2_FairShare
thrp1=`tail -n 2 thrp_SA2_DA1_AC1_ID_30_saturation__End.m  | grep 100000.0 | awk '{print $6}'`
thrp2=`tail -n 2 thrp_SA3_DA1_AC1_ID_44_saturation__End.m  | grep 100000.0 | awk '{print $6}'`
thrp3=`tail -n 2 thrp_SA4_DA1_AC1_ID_58_saturation__End.m  | grep 100000.0 | awk '{print $6}'`
thrp4=`tail -n 2 thrp_SA5_DA1_AC1_ID_72_saturation__End.m  | grep 100000.0 | awk '{print $6}'`
thrp5=`tail -n 2 thrp_SA6_DA1_AC1_ID_86_saturation__End.m  | grep 100000.0 | awk '{print $6}'`
thrp6=`tail -n 2 thrp_SA7_DA1_AC1_ID_100_saturation__End.m | grep 100000.0 | awk '{print $6}'`
echo Classic2, FairShare, $thrp1, $thrp2, $thrp3, $thrp4, $thrp5, $thrp6
cd ..

cd Classic2_FindingNash
thrp1=`tail -n 2 thrp_SA2_DA1_AC1_ID_30_saturation__End.m  | grep 100000.0 | awk '{print $6}'`
thrp2=`tail -n 2 thrp_SA3_DA1_AC1_ID_44_saturation__End.m  | grep 100000.0 | awk '{print $6}'`
thrp3=`tail -n 2 thrp_SA4_DA1_AC1_ID_58_saturation__End.m  | grep 100000.0 | awk '{print $6}'`
thrp4=`tail -n 2 thrp_SA5_DA1_AC1_ID_72_saturation__End.m  | grep 100000.0 | awk '{print $6}'`
thrp5=`tail -n 2 thrp_SA6_DA1_AC1_ID_86_saturation__End.m  | grep 100000.0 | awk '{print $6}'`
thrp6=`tail -n 2 thrp_SA7_DA1_AC1_ID_100_saturation__End.m | grep 100000.0 | awk '{print $6}'`
echo Classic2, FindingNash, $thrp1, $thrp2, $thrp3, $thrp4, $thrp5, $thrp6
cd ..

cd Classic2_Freeze
thrp1=`tail -n 2 thrp_SA2_DA1_AC1_ID_30_saturation__End.m  | grep 100000.0 | awk '{print $6}'`
thrp2=`tail -n 2 thrp_SA3_DA1_AC1_ID_44_saturation__End.m  | grep 100000.0 | awk '{print $6}'`
thrp3=`tail -n 2 thrp_SA4_DA1_AC1_ID_58_saturation__End.m  | grep 100000.0 | awk '{print $6}'`
thrp4=`tail -n 2 thrp_SA5_DA1_AC1_ID_72_saturation__End.m  | grep 100000.0 | awk '{print $6}'`
thrp5=`tail -n 2 thrp_SA6_DA1_AC1_ID_86_saturation__End.m  | grep 100000.0 | awk '{print $6}'`
thrp6=`tail -n 2 thrp_SA7_DA1_AC1_ID_100_saturation__End.m | grep 100000.0 | awk '{print $6}'`
echo Classic2, Freeze, $thrp1, $thrp2, $thrp3, $thrp4, $thrp5, $thrp6
cd ..

cd Classic2_None
thrp1=`tail -n 2 thrp_SA2_DA1_AC1_ID_30_saturation__End.m  | grep 100000.0 | awk '{print $6}'`
thrp2=`tail -n 2 thrp_SA3_DA1_AC1_ID_44_saturation__End.m  | grep 100000.0 | awk '{print $6}'`
thrp3=`tail -n 2 thrp_SA4_DA1_AC1_ID_58_saturation__End.m  | grep 100000.0 | awk '{print $6}'`
thrp4=`tail -n 2 thrp_SA5_DA1_AC1_ID_72_saturation__End.m  | grep 100000.0 | awk '{print $6}'`
thrp5=`tail -n 2 thrp_SA6_DA1_AC1_ID_86_saturation__End.m  | grep 100000.0 | awk '{print $6}'`
thrp6=`tail -n 2 thrp_SA7_DA1_AC1_ID_100_saturation__End.m | grep 100000.0 | awk '{print $6}'`
echo Classic2, None, $thrp1, $thrp2, $thrp3, $thrp4, $thrp5, $thrp6
cd ..

cd Classic2_PFA
thrp1=`tail -n 2 thrp_SA2_DA1_AC1_ID_30_saturation__End.m  | grep 100000.0 | awk '{print $6}'`
thrp2=`tail -n 2 thrp_SA3_DA1_AC1_ID_44_saturation__End.m  | grep 100000.0 | awk '{print $6}'`
thrp3=`tail -n 2 thrp_SA4_DA1_AC1_ID_58_saturation__End.m  | grep 100000.0 | awk '{print $6}'`
thrp4=`tail -n 2 thrp_SA5_DA1_AC1_ID_72_saturation__End.m  | grep 100000.0 | awk '{print $6}'`
thrp5=`tail -n 2 thrp_SA6_DA1_AC1_ID_86_saturation__End.m  | grep 100000.0 | awk '{print $6}'`
thrp6=`tail -n 2 thrp_SA7_DA1_AC1_ID_100_saturation__End.m | grep 100000.0 | awk '{print $6}'`
echo Classic2, PFA, $thrp1, $thrp2, $thrp3, $thrp4, $thrp5, $thrp6
cd ..

cd Classic2_Sneaky
thrp1=`tail -n 2 thrp_SA2_DA1_AC1_ID_30_saturation__End.m  | grep 100000.0 | awk '{print $6}'`
thrp2=`tail -n 2 thrp_SA3_DA1_AC1_ID_44_saturation__End.m  | grep 100000.0 | awk '{print $6}'`
thrp3=`tail -n 2 thrp_SA4_DA1_AC1_ID_58_saturation__End.m  | grep 100000.0 | awk '{print $6}'`
thrp4=`tail -n 2 thrp_SA5_DA1_AC1_ID_72_saturation__End.m  | grep 100000.0 | awk '{print $6}'`
thrp5=`tail -n 2 thrp_SA6_DA1_AC1_ID_86_saturation__End.m  | grep 100000.0 | awk '{print $6}'`
thrp6=`tail -n 2 thrp_SA7_DA1_AC1_ID_100_saturation__End.m | grep 100000.0 | awk '{print $6}'`
echo Classic2, Sneaky, $thrp1, $thrp2, $thrp3, $thrp4, $thrp5, $thrp6
cd ..

cd Classic2_StarFighter
thrp1=`tail -n 2 thrp_SA2_DA1_AC1_ID_30_saturation__End.m  | grep 100000.0 | awk '{print $6}'`
thrp2=`tail -n 2 thrp_SA3_DA1_AC1_ID_44_saturation__End.m  | grep 100000.0 | awk '{print $6}'`
thrp3=`tail -n 2 thrp_SA4_DA1_AC1_ID_58_saturation__End.m  | grep 100000.0 | awk '{print $6}'`
thrp4=`tail -n 2 thrp_SA5_DA1_AC1_ID_72_saturation__End.m  | grep 100000.0 | awk '{print $6}'`
thrp5=`tail -n 2 thrp_SA6_DA1_AC1_ID_86_saturation__End.m  | grep 100000.0 | awk '{print $6}'`
thrp6=`tail -n 2 thrp_SA7_DA1_AC1_ID_100_saturation__End.m | grep 100000.0 | awk '{print $6}'`
echo Classic2, StarFighter, $thrp1, $thrp2, $thrp3, $thrp4, $thrp5, $thrp6
cd ..

cd Classic2_Vincent
thrp1=`tail -n 2 thrp_SA2_DA1_AC1_ID_30_saturation__End.m  | grep 100000.0 | awk '{print $6}'`
thrp2=`tail -n 2 thrp_SA3_DA1_AC1_ID_44_saturation__End.m  | grep 100000.0 | awk '{print $6}'`
thrp3=`tail -n 2 thrp_SA4_DA1_AC1_ID_58_saturation__End.m  | grep 100000.0 | awk '{print $6}'`
thrp4=`tail -n 2 thrp_SA5_DA1_AC1_ID_72_saturation__End.m  | grep 100000.0 | awk '{print $6}'`
thrp5=`tail -n 2 thrp_SA6_DA1_AC1_ID_86_saturation__End.m  | grep 100000.0 | awk '{print $6}'`
thrp6=`tail -n 2 thrp_SA7_DA1_AC1_ID_100_saturation__End.m | grep 100000.0 | awk '{print $6}'`
echo Classic2, Vincent, $thrp1, $thrp2, $thrp3, $thrp4, $thrp5, $thrp6
cd ..

cd Classic2_Yacmoca
thrp1=`tail -n 2 thrp_SA2_DA1_AC1_ID_30_saturation__End.m  | grep 100000.0 | awk '{print $6}'`
thrp2=`tail -n 2 thrp_SA3_DA1_AC1_ID_44_saturation__End.m  | grep 100000.0 | awk '{print $6}'`
thrp3=`tail -n 2 thrp_SA4_DA1_AC1_ID_58_saturation__End.m  | grep 100000.0 | awk '{print $6}'`
thrp4=`tail -n 2 thrp_SA5_DA1_AC1_ID_72_saturation__End.m  | grep 100000.0 | awk '{print $6}'`
thrp5=`tail -n 2 thrp_SA6_DA1_AC1_ID_86_saturation__End.m  | grep 100000.0 | awk '{print $6}'`
thrp6=`tail -n 2 thrp_SA7_DA1_AC1_ID_100_saturation__End.m | grep 100000.0 | awk '{print $6}'`
echo Classic2, Yacmoca, $thrp1, $thrp2, $thrp3, $thrp4, $thrp5, $thrp6
cd ..

cd FairShare_FairShare
thrp1=`tail -n 2 thrp_SA2_DA1_AC1_ID_30_saturation__End.m  | grep 100000.0 | awk '{print $6}'`
thrp2=`tail -n 2 thrp_SA3_DA1_AC1_ID_44_saturation__End.m  | grep 100000.0 | awk '{print $6}'`
thrp3=`tail -n 2 thrp_SA4_DA1_AC1_ID_58_saturation__End.m  | grep 100000.0 | awk '{print $6}'`
thrp4=`tail -n 2 thrp_SA5_DA1_AC1_ID_72_saturation__End.m  | grep 100000.0 | awk '{print $6}'`
thrp5=`tail -n 2 thrp_SA6_DA1_AC1_ID_86_saturation__End.m  | grep 100000.0 | awk '{print $6}'`
thrp6=`tail -n 2 thrp_SA7_DA1_AC1_ID_100_saturation__End.m | grep 100000.0 | awk '{print $6}'`
echo FairShare, FairShare, $thrp1, $thrp2, $thrp3, $thrp4, $thrp5, $thrp6
cd ..

cd FairShare_FindingNash
thrp1=`tail -n 2 thrp_SA2_DA1_AC1_ID_30_saturation__End.m  | grep 100000.0 | awk '{print $6}'`
thrp2=`tail -n 2 thrp_SA3_DA1_AC1_ID_44_saturation__End.m  | grep 100000.0 | awk '{print $6}'`
thrp3=`tail -n 2 thrp_SA4_DA1_AC1_ID_58_saturation__End.m  | grep 100000.0 | awk '{print $6}'`
thrp4=`tail -n 2 thrp_SA5_DA1_AC1_ID_72_saturation__End.m  | grep 100000.0 | awk '{print $6}'`
thrp5=`tail -n 2 thrp_SA6_DA1_AC1_ID_86_saturation__End.m  | grep 100000.0 | awk '{print $6}'`
thrp6=`tail -n 2 thrp_SA7_DA1_AC1_ID_100_saturation__End.m | grep 100000.0 | awk '{print $6}'`
echo FairShare, FindingNash, $thrp1, $thrp2, $thrp3, $thrp4, $thrp5, $thrp6
cd ..

cd FairShare_Freeze
thrp1=`tail -n 2 thrp_SA2_DA1_AC1_ID_30_saturation__End.m  | grep 100000.0 | awk '{print $6}'`
thrp2=`tail -n 2 thrp_SA3_DA1_AC1_ID_44_saturation__End.m  | grep 100000.0 | awk '{print $6}'`
thrp3=`tail -n 2 thrp_SA4_DA1_AC1_ID_58_saturation__End.m  | grep 100000.0 | awk '{print $6}'`
thrp4=`tail -n 2 thrp_SA5_DA1_AC1_ID_72_saturation__End.m  | grep 100000.0 | awk '{print $6}'`
thrp5=`tail -n 2 thrp_SA6_DA1_AC1_ID_86_saturation__End.m  | grep 100000.0 | awk '{print $6}'`
thrp6=`tail -n 2 thrp_SA7_DA1_AC1_ID_100_saturation__End.m | grep 100000.0 | awk '{print $6}'`
echo FairShare, Freeze, $thrp1, $thrp2, $thrp3, $thrp4, $thrp5, $thrp6
cd ..

cd FairShare_None
thrp1=`tail -n 2 thrp_SA2_DA1_AC1_ID_30_saturation__End.m  | grep 100000.0 | awk '{print $6}'`
thrp2=`tail -n 2 thrp_SA3_DA1_AC1_ID_44_saturation__End.m  | grep 100000.0 | awk '{print $6}'`
thrp3=`tail -n 2 thrp_SA4_DA1_AC1_ID_58_saturation__End.m  | grep 100000.0 | awk '{print $6}'`
thrp4=`tail -n 2 thrp_SA5_DA1_AC1_ID_72_saturation__End.m  | grep 100000.0 | awk '{print $6}'`
thrp5=`tail -n 2 thrp_SA6_DA1_AC1_ID_86_saturation__End.m  | grep 100000.0 | awk '{print $6}'`
thrp6=`tail -n 2 thrp_SA7_DA1_AC1_ID_100_saturation__End.m | grep 100000.0 | awk '{print $6}'`
echo FairShare, None, $thrp1, $thrp2, $thrp3, $thrp4, $thrp5, $thrp6
cd ..

cd FairShare_PFA
thrp1=`tail -n 2 thrp_SA2_DA1_AC1_ID_30_saturation__End.m  | grep 100000.0 | awk '{print $6}'`
thrp2=`tail -n 2 thrp_SA3_DA1_AC1_ID_44_saturation__End.m  | grep 100000.0 | awk '{print $6}'`
thrp3=`tail -n 2 thrp_SA4_DA1_AC1_ID_58_saturation__End.m  | grep 100000.0 | awk '{print $6}'`
thrp4=`tail -n 2 thrp_SA5_DA1_AC1_ID_72_saturation__End.m  | grep 100000.0 | awk '{print $6}'`
thrp5=`tail -n 2 thrp_SA6_DA1_AC1_ID_86_saturation__End.m  | grep 100000.0 | awk '{print $6}'`
thrp6=`tail -n 2 thrp_SA7_DA1_AC1_ID_100_saturation__End.m | grep 100000.0 | awk '{print $6}'`
echo FairShare, PFA, $thrp1, $thrp2, $thrp3, $thrp4, $thrp5, $thrp6
cd ..

cd FairShare_Sneaky
thrp1=`tail -n 2 thrp_SA2_DA1_AC1_ID_30_saturation__End.m  | grep 100000.0 | awk '{print $6}'`
thrp2=`tail -n 2 thrp_SA3_DA1_AC1_ID_44_saturation__End.m  | grep 100000.0 | awk '{print $6}'`
thrp3=`tail -n 2 thrp_SA4_DA1_AC1_ID_58_saturation__End.m  | grep 100000.0 | awk '{print $6}'`
thrp4=`tail -n 2 thrp_SA5_DA1_AC1_ID_72_saturation__End.m  | grep 100000.0 | awk '{print $6}'`
thrp5=`tail -n 2 thrp_SA6_DA1_AC1_ID_86_saturation__End.m  | grep 100000.0 | awk '{print $6}'`
thrp6=`tail -n 2 thrp_SA7_DA1_AC1_ID_100_saturation__End.m | grep 100000.0 | awk '{print $6}'`
echo FairShare, Sneaky, $thrp1, $thrp2, $thrp3, $thrp4, $thrp5, $thrp6
cd ..

cd FairShare_StarFighter
thrp1=`tail -n 2 thrp_SA2_DA1_AC1_ID_30_saturation__End.m  | grep 100000.0 | awk '{print $6}'`
thrp2=`tail -n 2 thrp_SA3_DA1_AC1_ID_44_saturation__End.m  | grep 100000.0 | awk '{print $6}'`
thrp3=`tail -n 2 thrp_SA4_DA1_AC1_ID_58_saturation__End.m  | grep 100000.0 | awk '{print $6}'`
thrp4=`tail -n 2 thrp_SA5_DA1_AC1_ID_72_saturation__End.m  | grep 100000.0 | awk '{print $6}'`
thrp5=`tail -n 2 thrp_SA6_DA1_AC1_ID_86_saturation__End.m  | grep 100000.0 | awk '{print $6}'`
thrp6=`tail -n 2 thrp_SA7_DA1_AC1_ID_100_saturation__End.m | grep 100000.0 | awk '{print $6}'`
echo FairShare, StarFighter, $thrp1, $thrp2, $thrp3, $thrp4, $thrp5, $thrp6
cd ..

cd FairShare_Vincent
thrp1=`tail -n 2 thrp_SA2_DA1_AC1_ID_30_saturation__End.m  | grep 100000.0 | awk '{print $6}'`
thrp2=`tail -n 2 thrp_SA3_DA1_AC1_ID_44_saturation__End.m  | grep 100000.0 | awk '{print $6}'`
thrp3=`tail -n 2 thrp_SA4_DA1_AC1_ID_58_saturation__End.m  | grep 100000.0 | awk '{print $6}'`
thrp4=`tail -n 2 thrp_SA5_DA1_AC1_ID_72_saturation__End.m  | grep 100000.0 | awk '{print $6}'`
thrp5=`tail -n 2 thrp_SA6_DA1_AC1_ID_86_saturation__End.m  | grep 100000.0 | awk '{print $6}'`
thrp6=`tail -n 2 thrp_SA7_DA1_AC1_ID_100_saturation__End.m | grep 100000.0 | awk '{print $6}'`
echo FairShare, Vincent, $thrp1, $thrp2, $thrp3, $thrp4, $thrp5, $thrp6
cd ..

cd FairShare_Yacmoca
thrp1=`tail -n 2 thrp_SA2_DA1_AC1_ID_30_saturation__End.m  | grep 100000.0 | awk '{print $6}'`
thrp2=`tail -n 2 thrp_SA3_DA1_AC1_ID_44_saturation__End.m  | grep 100000.0 | awk '{print $6}'`
thrp3=`tail -n 2 thrp_SA4_DA1_AC1_ID_58_saturation__End.m  | grep 100000.0 | awk '{print $6}'`
thrp4=`tail -n 2 thrp_SA5_DA1_AC1_ID_72_saturation__End.m  | grep 100000.0 | awk '{print $6}'`
thrp5=`tail -n 2 thrp_SA6_DA1_AC1_ID_86_saturation__End.m  | grep 100000.0 | awk '{print $6}'`
thrp6=`tail -n 2 thrp_SA7_DA1_AC1_ID_100_saturation__End.m | grep 100000.0 | awk '{print $6}'`
echo FairShare, Yacmoca, $thrp1, $thrp2, $thrp3, $thrp4, $thrp5, $thrp6
cd ..

cd FindingNash_FindingNash
thrp1=`tail -n 2 thrp_SA2_DA1_AC1_ID_30_saturation__End.m  | grep 100000.0 | awk '{print $6}'`
thrp2=`tail -n 2 thrp_SA3_DA1_AC1_ID_44_saturation__End.m  | grep 100000.0 | awk '{print $6}'`
thrp3=`tail -n 2 thrp_SA4_DA1_AC1_ID_58_saturation__End.m  | grep 100000.0 | awk '{print $6}'`
thrp4=`tail -n 2 thrp_SA5_DA1_AC1_ID_72_saturation__End.m  | grep 100000.0 | awk '{print $6}'`
thrp5=`tail -n 2 thrp_SA6_DA1_AC1_ID_86_saturation__End.m  | grep 100000.0 | awk '{print $6}'`
thrp6=`tail -n 2 thrp_SA7_DA1_AC1_ID_100_saturation__End.m | grep 100000.0 | awk '{print $6}'`
echo FindingNash, FindingNash, $thrp1, $thrp2, $thrp3, $thrp4, $thrp5, $thrp6
cd ..

cd FindingNash_Freeze
thrp1=`tail -n 2 thrp_SA2_DA1_AC1_ID_30_saturation__End.m  | grep 100000.0 | awk '{print $6}'`
thrp2=`tail -n 2 thrp_SA3_DA1_AC1_ID_44_saturation__End.m  | grep 100000.0 | awk '{print $6}'`
thrp3=`tail -n 2 thrp_SA4_DA1_AC1_ID_58_saturation__End.m  | grep 100000.0 | awk '{print $6}'`
thrp4=`tail -n 2 thrp_SA5_DA1_AC1_ID_72_saturation__End.m  | grep 100000.0 | awk '{print $6}'`
thrp5=`tail -n 2 thrp_SA6_DA1_AC1_ID_86_saturation__End.m  | grep 100000.0 | awk '{print $6}'`
thrp6=`tail -n 2 thrp_SA7_DA1_AC1_ID_100_saturation__End.m | grep 100000.0 | awk '{print $6}'`
echo FindingNash, Freeze, $thrp1, $thrp2, $thrp3, $thrp4, $thrp5, $thrp6
cd ..

cd FindingNash_None
thrp1=`tail -n 2 thrp_SA2_DA1_AC1_ID_30_saturation__End.m  | grep 100000.0 | awk '{print $6}'`
thrp2=`tail -n 2 thrp_SA3_DA1_AC1_ID_44_saturation__End.m  | grep 100000.0 | awk '{print $6}'`
thrp3=`tail -n 2 thrp_SA4_DA1_AC1_ID_58_saturation__End.m  | grep 100000.0 | awk '{print $6}'`
thrp4=`tail -n 2 thrp_SA5_DA1_AC1_ID_72_saturation__End.m  | grep 100000.0 | awk '{print $6}'`
thrp5=`tail -n 2 thrp_SA6_DA1_AC1_ID_86_saturation__End.m  | grep 100000.0 | awk '{print $6}'`
thrp6=`tail -n 2 thrp_SA7_DA1_AC1_ID_100_saturation__End.m | grep 100000.0 | awk '{print $6}'`
echo FindingNash, None, $thrp1, $thrp2, $thrp3, $thrp4, $thrp5, $thrp6
cd ..

cd FindingNash_PFA
thrp1=`tail -n 2 thrp_SA2_DA1_AC1_ID_30_saturation__End.m  | grep 100000.0 | awk '{print $6}'`
thrp2=`tail -n 2 thrp_SA3_DA1_AC1_ID_44_saturation__End.m  | grep 100000.0 | awk '{print $6}'`
thrp3=`tail -n 2 thrp_SA4_DA1_AC1_ID_58_saturation__End.m  | grep 100000.0 | awk '{print $6}'`
thrp4=`tail -n 2 thrp_SA5_DA1_AC1_ID_72_saturation__End.m  | grep 100000.0 | awk '{print $6}'`
thrp5=`tail -n 2 thrp_SA6_DA1_AC1_ID_86_saturation__End.m  | grep 100000.0 | awk '{print $6}'`
thrp6=`tail -n 2 thrp_SA7_DA1_AC1_ID_100_saturation__End.m | grep 100000.0 | awk '{print $6}'`
echo FindingNash, PFA, $thrp1, $thrp2, $thrp3, $thrp4, $thrp5, $thrp6
cd ..

cd FindingNash_Sneaky
thrp1=`tail -n 2 thrp_SA2_DA1_AC1_ID_30_saturation__End.m  | grep 100000.0 | awk '{print $6}'`
thrp2=`tail -n 2 thrp_SA3_DA1_AC1_ID_44_saturation__End.m  | grep 100000.0 | awk '{print $6}'`
thrp3=`tail -n 2 thrp_SA4_DA1_AC1_ID_58_saturation__End.m  | grep 100000.0 | awk '{print $6}'`
thrp4=`tail -n 2 thrp_SA5_DA1_AC1_ID_72_saturation__End.m  | grep 100000.0 | awk '{print $6}'`
thrp5=`tail -n 2 thrp_SA6_DA1_AC1_ID_86_saturation__End.m  | grep 100000.0 | awk '{print $6}'`
thrp6=`tail -n 2 thrp_SA7_DA1_AC1_ID_100_saturation__End.m | grep 100000.0 | awk '{print $6}'`
echo FindingNash, Sneaky, $thrp1, $thrp2, $thrp3, $thrp4, $thrp5, $thrp6
cd ..

cd FindingNash_StarFighter
thrp1=`tail -n 2 thrp_SA2_DA1_AC1_ID_30_saturation__End.m  | grep 100000.0 | awk '{print $6}'`
thrp2=`tail -n 2 thrp_SA3_DA1_AC1_ID_44_saturation__End.m  | grep 100000.0 | awk '{print $6}'`
thrp3=`tail -n 2 thrp_SA4_DA1_AC1_ID_58_saturation__End.m  | grep 100000.0 | awk '{print $6}'`
thrp4=`tail -n 2 thrp_SA5_DA1_AC1_ID_72_saturation__End.m  | grep 100000.0 | awk '{print $6}'`
thrp5=`tail -n 2 thrp_SA6_DA1_AC1_ID_86_saturation__End.m  | grep 100000.0 | awk '{print $6}'`
thrp6=`tail -n 2 thrp_SA7_DA1_AC1_ID_100_saturation__End.m | grep 100000.0 | awk '{print $6}'`
echo FindingNash, StarFighter, $thrp1, $thrp2, $thrp3, $thrp4, $thrp5, $thrp6
cd ..

cd FindingNash_Vincent
thrp1=`tail -n 2 thrp_SA2_DA1_AC1_ID_30_saturation__End.m  | grep 100000.0 | awk '{print $6}'`
thrp2=`tail -n 2 thrp_SA3_DA1_AC1_ID_44_saturation__End.m  | grep 100000.0 | awk '{print $6}'`
thrp3=`tail -n 2 thrp_SA4_DA1_AC1_ID_58_saturation__End.m  | grep 100000.0 | awk '{print $6}'`
thrp4=`tail -n 2 thrp_SA5_DA1_AC1_ID_72_saturation__End.m  | grep 100000.0 | awk '{print $6}'`
thrp5=`tail -n 2 thrp_SA6_DA1_AC1_ID_86_saturation__End.m  | grep 100000.0 | awk '{print $6}'`
thrp6=`tail -n 2 thrp_SA7_DA1_AC1_ID_100_saturation__End.m | grep 100000.0 | awk '{print $6}'`
echo FindingNash, Vincent, $thrp1, $thrp2, $thrp3, $thrp4, $thrp5, $thrp6
cd ..

cd FindingNash_Yacmoca
thrp1=`tail -n 2 thrp_SA2_DA1_AC1_ID_30_saturation__End.m  | grep 100000.0 | awk '{print $6}'`
thrp2=`tail -n 2 thrp_SA3_DA1_AC1_ID_44_saturation__End.m  | grep 100000.0 | awk '{print $6}'`
thrp3=`tail -n 2 thrp_SA4_DA1_AC1_ID_58_saturation__End.m  | grep 100000.0 | awk '{print $6}'`
thrp4=`tail -n 2 thrp_SA5_DA1_AC1_ID_72_saturation__End.m  | grep 100000.0 | awk '{print $6}'`
thrp5=`tail -n 2 thrp_SA6_DA1_AC1_ID_86_saturation__End.m  | grep 100000.0 | awk '{print $6}'`
thrp6=`tail -n 2 thrp_SA7_DA1_AC1_ID_100_saturation__End.m | grep 100000.0 | awk '{print $6}'`
echo FindingNash, Yacmoca, $thrp1, $thrp2, $thrp3, $thrp4, $thrp5, $thrp6
cd ..

cd Freeze_Freeze
thrp1=`tail -n 2 thrp_SA2_DA1_AC1_ID_30_saturation__End.m  | grep 100000.0 | awk '{print $6}'`
thrp2=`tail -n 2 thrp_SA3_DA1_AC1_ID_44_saturation__End.m  | grep 100000.0 | awk '{print $6}'`
thrp3=`tail -n 2 thrp_SA4_DA1_AC1_ID_58_saturation__End.m  | grep 100000.0 | awk '{print $6}'`
thrp4=`tail -n 2 thrp_SA5_DA1_AC1_ID_72_saturation__End.m  | grep 100000.0 | awk '{print $6}'`
thrp5=`tail -n 2 thrp_SA6_DA1_AC1_ID_86_saturation__End.m  | grep 100000.0 | awk '{print $6}'`
thrp6=`tail -n 2 thrp_SA7_DA1_AC1_ID_100_saturation__End.m | grep 100000.0 | awk '{print $6}'`
echo Freeze, Freeze, $thrp1, $thrp2, $thrp3, $thrp4, $thrp5, $thrp6
cd ..

cd Freeze_None
thrp1=`tail -n 2 thrp_SA2_DA1_AC1_ID_30_saturation__End.m  | grep 100000.0 | awk '{print $6}'`
thrp2=`tail -n 2 thrp_SA3_DA1_AC1_ID_44_saturation__End.m  | grep 100000.0 | awk '{print $6}'`
thrp3=`tail -n 2 thrp_SA4_DA1_AC1_ID_58_saturation__End.m  | grep 100000.0 | awk '{print $6}'`
thrp4=`tail -n 2 thrp_SA5_DA1_AC1_ID_72_saturation__End.m  | grep 100000.0 | awk '{print $6}'`
thrp5=`tail -n 2 thrp_SA6_DA1_AC1_ID_86_saturation__End.m  | grep 100000.0 | awk '{print $6}'`
thrp6=`tail -n 2 thrp_SA7_DA1_AC1_ID_100_saturation__End.m | grep 100000.0 | awk '{print $6}'`
echo Freeze, None, $thrp1, $thrp2, $thrp3, $thrp4, $thrp5, $thrp6
cd ..

cd Freeze_PFA
thrp1=`tail -n 2 thrp_SA2_DA1_AC1_ID_30_saturation__End.m  | grep 100000.0 | awk '{print $6}'`
thrp2=`tail -n 2 thrp_SA3_DA1_AC1_ID_44_saturation__End.m  | grep 100000.0 | awk '{print $6}'`
thrp3=`tail -n 2 thrp_SA4_DA1_AC1_ID_58_saturation__End.m  | grep 100000.0 | awk '{print $6}'`
thrp4=`tail -n 2 thrp_SA5_DA1_AC1_ID_72_saturation__End.m  | grep 100000.0 | awk '{print $6}'`
thrp5=`tail -n 2 thrp_SA6_DA1_AC1_ID_86_saturation__End.m  | grep 100000.0 | awk '{print $6}'`
thrp6=`tail -n 2 thrp_SA7_DA1_AC1_ID_100_saturation__End.m | grep 100000.0 | awk '{print $6}'`
echo Freeze, PFA, $thrp1, $thrp2, $thrp3, $thrp4, $thrp5, $thrp6
cd ..

cd Freeze_Sneaky
thrp1=`tail -n 2 thrp_SA2_DA1_AC1_ID_30_saturation__End.m  | grep 100000.0 | awk '{print $6}'`
thrp2=`tail -n 2 thrp_SA3_DA1_AC1_ID_44_saturation__End.m  | grep 100000.0 | awk '{print $6}'`
thrp3=`tail -n 2 thrp_SA4_DA1_AC1_ID_58_saturation__End.m  | grep 100000.0 | awk '{print $6}'`
thrp4=`tail -n 2 thrp_SA5_DA1_AC1_ID_72_saturation__End.m  | grep 100000.0 | awk '{print $6}'`
thrp5=`tail -n 2 thrp_SA6_DA1_AC1_ID_86_saturation__End.m  | grep 100000.0 | awk '{print $6}'`
thrp6=`tail -n 2 thrp_SA7_DA1_AC1_ID_100_saturation__End.m | grep 100000.0 | awk '{print $6}'`
echo Freeze, Sneaky, $thrp1, $thrp2, $thrp3, $thrp4, $thrp5, $thrp6
cd ..

cd Freeze_StarFighter
thrp1=`tail -n 2 thrp_SA2_DA1_AC1_ID_30_saturation__End.m  | grep 100000.0 | awk '{print $6}'`
thrp2=`tail -n 2 thrp_SA3_DA1_AC1_ID_44_saturation__End.m  | grep 100000.0 | awk '{print $6}'`
thrp3=`tail -n 2 thrp_SA4_DA1_AC1_ID_58_saturation__End.m  | grep 100000.0 | awk '{print $6}'`
thrp4=`tail -n 2 thrp_SA5_DA1_AC1_ID_72_saturation__End.m  | grep 100000.0 | awk '{print $6}'`
thrp5=`tail -n 2 thrp_SA6_DA1_AC1_ID_86_saturation__End.m  | grep 100000.0 | awk '{print $6}'`
thrp6=`tail -n 2 thrp_SA7_DA1_AC1_ID_100_saturation__End.m | grep 100000.0 | awk '{print $6}'`
echo Freeze, StarFighter, $thrp1, $thrp2, $thrp3, $thrp4, $thrp5, $thrp6
cd ..

cd Freeze_Vincent
thrp1=`tail -n 2 thrp_SA2_DA1_AC1_ID_30_saturation__End.m  | grep 100000.0 | awk '{print $6}'`
thrp2=`tail -n 2 thrp_SA3_DA1_AC1_ID_44_saturation__End.m  | grep 100000.0 | awk '{print $6}'`
thrp3=`tail -n 2 thrp_SA4_DA1_AC1_ID_58_saturation__End.m  | grep 100000.0 | awk '{print $6}'`
thrp4=`tail -n 2 thrp_SA5_DA1_AC1_ID_72_saturation__End.m  | grep 100000.0 | awk '{print $6}'`
thrp5=`tail -n 2 thrp_SA6_DA1_AC1_ID_86_saturation__End.m  | grep 100000.0 | awk '{print $6}'`
thrp6=`tail -n 2 thrp_SA7_DA1_AC1_ID_100_saturation__End.m | grep 100000.0 | awk '{print $6}'`
echo Freeze, Vincent, $thrp1, $thrp2, $thrp3, $thrp4, $thrp5, $thrp6
cd ..

cd Freeze_Yacmoca
thrp1=`tail -n 2 thrp_SA2_DA1_AC1_ID_30_saturation__End.m  | grep 100000.0 | awk '{print $6}'`
thrp2=`tail -n 2 thrp_SA3_DA1_AC1_ID_44_saturation__End.m  | grep 100000.0 | awk '{print $6}'`
thrp3=`tail -n 2 thrp_SA4_DA1_AC1_ID_58_saturation__End.m  | grep 100000.0 | awk '{print $6}'`
thrp4=`tail -n 2 thrp_SA5_DA1_AC1_ID_72_saturation__End.m  | grep 100000.0 | awk '{print $6}'`
thrp5=`tail -n 2 thrp_SA6_DA1_AC1_ID_86_saturation__End.m  | grep 100000.0 | awk '{print $6}'`
thrp6=`tail -n 2 thrp_SA7_DA1_AC1_ID_100_saturation__End.m | grep 100000.0 | awk '{print $6}'`
echo Freeze, Yacmoca, $thrp1, $thrp2, $thrp3, $thrp4, $thrp5, $thrp6
cd ..

cd None_None
thrp1=`tail -n 2 thrp_SA2_DA1_AC1_ID_30_saturation__End.m  | grep 100000.0 | awk '{print $6}'`
thrp2=`tail -n 2 thrp_SA3_DA1_AC1_ID_44_saturation__End.m  | grep 100000.0 | awk '{print $6}'`
thrp3=`tail -n 2 thrp_SA4_DA1_AC1_ID_58_saturation__End.m  | grep 100000.0 | awk '{print $6}'`
thrp4=`tail -n 2 thrp_SA5_DA1_AC1_ID_72_saturation__End.m  | grep 100000.0 | awk '{print $6}'`
thrp5=`tail -n 2 thrp_SA6_DA1_AC1_ID_86_saturation__End.m  | grep 100000.0 | awk '{print $6}'`
thrp6=`tail -n 2 thrp_SA7_DA1_AC1_ID_100_saturation__End.m | grep 100000.0 | awk '{print $6}'`
echo None, None, $thrp1, $thrp2, $thrp3, $thrp4, $thrp5, $thrp6
cd ..

cd None_PFA
thrp1=`tail -n 2 thrp_SA2_DA1_AC1_ID_30_saturation__End.m  | grep 100000.0 | awk '{print $6}'`
thrp2=`tail -n 2 thrp_SA3_DA1_AC1_ID_44_saturation__End.m  | grep 100000.0 | awk '{print $6}'`
thrp3=`tail -n 2 thrp_SA4_DA1_AC1_ID_58_saturation__End.m  | grep 100000.0 | awk '{print $6}'`
thrp4=`tail -n 2 thrp_SA5_DA1_AC1_ID_72_saturation__End.m  | grep 100000.0 | awk '{print $6}'`
thrp5=`tail -n 2 thrp_SA6_DA1_AC1_ID_86_saturation__End.m  | grep 100000.0 | awk '{print $6}'`
thrp6=`tail -n 2 thrp_SA7_DA1_AC1_ID_100_saturation__End.m | grep 100000.0 | awk '{print $6}'`
echo None, PFA, $thrp1, $thrp2, $thrp3, $thrp4, $thrp5, $thrp6
cd ..

cd None_Sneaky
thrp1=`tail -n 2 thrp_SA2_DA1_AC1_ID_30_saturation__End.m  | grep 100000.0 | awk '{print $6}'`
thrp2=`tail -n 2 thrp_SA3_DA1_AC1_ID_44_saturation__End.m  | grep 100000.0 | awk '{print $6}'`
thrp3=`tail -n 2 thrp_SA4_DA1_AC1_ID_58_saturation__End.m  | grep 100000.0 | awk '{print $6}'`
thrp4=`tail -n 2 thrp_SA5_DA1_AC1_ID_72_saturation__End.m  | grep 100000.0 | awk '{print $6}'`
thrp5=`tail -n 2 thrp_SA6_DA1_AC1_ID_86_saturation__End.m  | grep 100000.0 | awk '{print $6}'`
thrp6=`tail -n 2 thrp_SA7_DA1_AC1_ID_100_saturation__End.m | grep 100000.0 | awk '{print $6}'`
echo None, Sneaky, $thrp1, $thrp2, $thrp3, $thrp4, $thrp5, $thrp6
cd ..

cd None_StarFighter
thrp1=`tail -n 2 thrp_SA2_DA1_AC1_ID_30_saturation__End.m  | grep 100000.0 | awk '{print $6}'`
thrp2=`tail -n 2 thrp_SA3_DA1_AC1_ID_44_saturation__End.m  | grep 100000.0 | awk '{print $6}'`
thrp3=`tail -n 2 thrp_SA4_DA1_AC1_ID_58_saturation__End.m  | grep 100000.0 | awk '{print $6}'`
thrp4=`tail -n 2 thrp_SA5_DA1_AC1_ID_72_saturation__End.m  | grep 100000.0 | awk '{print $6}'`
thrp5=`tail -n 2 thrp_SA6_DA1_AC1_ID_86_saturation__End.m  | grep 100000.0 | awk '{print $6}'`
thrp6=`tail -n 2 thrp_SA7_DA1_AC1_ID_100_saturation__End.m | grep 100000.0 | awk '{print $6}'`
echo None, StarFighter, $thrp1, $thrp2, $thrp3, $thrp4, $thrp5, $thrp6
cd ..

cd None_Vincent
thrp1=`tail -n 2 thrp_SA2_DA1_AC1_ID_30_saturation__End.m  | grep 100000.0 | awk '{print $6}'`
thrp2=`tail -n 2 thrp_SA3_DA1_AC1_ID_44_saturation__End.m  | grep 100000.0 | awk '{print $6}'`
thrp3=`tail -n 2 thrp_SA4_DA1_AC1_ID_58_saturation__End.m  | grep 100000.0 | awk '{print $6}'`
thrp4=`tail -n 2 thrp_SA5_DA1_AC1_ID_72_saturation__End.m  | grep 100000.0 | awk '{print $6}'`
thrp5=`tail -n 2 thrp_SA6_DA1_AC1_ID_86_saturation__End.m  | grep 100000.0 | awk '{print $6}'`
thrp6=`tail -n 2 thrp_SA7_DA1_AC1_ID_100_saturation__End.m | grep 100000.0 | awk '{print $6}'`
echo None, Vincent, $thrp1, $thrp2, $thrp3, $thrp4, $thrp5, $thrp6
cd ..

cd None_Yacmoca
thrp1=`tail -n 2 thrp_SA2_DA1_AC1_ID_30_saturation__End.m  | grep 100000.0 | awk '{print $6}'`
thrp2=`tail -n 2 thrp_SA3_DA1_AC1_ID_44_saturation__End.m  | grep 100000.0 | awk '{print $6}'`
thrp3=`tail -n 2 thrp_SA4_DA1_AC1_ID_58_saturation__End.m  | grep 100000.0 | awk '{print $6}'`
thrp4=`tail -n 2 thrp_SA5_DA1_AC1_ID_72_saturation__End.m  | grep 100000.0 | awk '{print $6}'`
thrp5=`tail -n 2 thrp_SA6_DA1_AC1_ID_86_saturation__End.m  | grep 100000.0 | awk '{print $6}'`
thrp6=`tail -n 2 thrp_SA7_DA1_AC1_ID_100_saturation__End.m | grep 100000.0 | awk '{print $6}'`
echo None, Yacmoca, $thrp1, $thrp2, $thrp3, $thrp4, $thrp5, $thrp6
cd ..

cd PFA_PFA
thrp1=`tail -n 2 thrp_SA2_DA1_AC1_ID_30_saturation__End.m  | grep 100000.0 | awk '{print $6}'`
thrp2=`tail -n 2 thrp_SA3_DA1_AC1_ID_44_saturation__End.m  | grep 100000.0 | awk '{print $6}'`
thrp3=`tail -n 2 thrp_SA4_DA1_AC1_ID_58_saturation__End.m  | grep 100000.0 | awk '{print $6}'`
thrp4=`tail -n 2 thrp_SA5_DA1_AC1_ID_72_saturation__End.m  | grep 100000.0 | awk '{print $6}'`
thrp5=`tail -n 2 thrp_SA6_DA1_AC1_ID_86_saturation__End.m  | grep 100000.0 | awk '{print $6}'`
thrp6=`tail -n 2 thrp_SA7_DA1_AC1_ID_100_saturation__End.m | grep 100000.0 | awk '{print $6}'`
echo PFA, PFA, $thrp1, $thrp2, $thrp3, $thrp4, $thrp5, $thrp6
cd ..

cd PFA_Sneaky
thrp1=`tail -n 2 thrp_SA2_DA1_AC1_ID_30_saturation__End.m  | grep 100000.0 | awk '{print $6}'`
thrp2=`tail -n 2 thrp_SA3_DA1_AC1_ID_44_saturation__End.m  | grep 100000.0 | awk '{print $6}'`
thrp3=`tail -n 2 thrp_SA4_DA1_AC1_ID_58_saturation__End.m  | grep 100000.0 | awk '{print $6}'`
thrp4=`tail -n 2 thrp_SA5_DA1_AC1_ID_72_saturation__End.m  | grep 100000.0 | awk '{print $6}'`
thrp5=`tail -n 2 thrp_SA6_DA1_AC1_ID_86_saturation__End.m  | grep 100000.0 | awk '{print $6}'`
thrp6=`tail -n 2 thrp_SA7_DA1_AC1_ID_100_saturation__End.m | grep 100000.0 | awk '{print $6}'`
echo PFA, Sneaky, $thrp1, $thrp2, $thrp3, $thrp4, $thrp5, $thrp6
cd ..

cd PFA_StarFighter
thrp1=`tail -n 2 thrp_SA2_DA1_AC1_ID_30_saturation__End.m  | grep 100000.0 | awk '{print $6}'`
thrp2=`tail -n 2 thrp_SA3_DA1_AC1_ID_44_saturation__End.m  | grep 100000.0 | awk '{print $6}'`
thrp3=`tail -n 2 thrp_SA4_DA1_AC1_ID_58_saturation__End.m  | grep 100000.0 | awk '{print $6}'`
thrp4=`tail -n 2 thrp_SA5_DA1_AC1_ID_72_saturation__End.m  | grep 100000.0 | awk '{print $6}'`
thrp5=`tail -n 2 thrp_SA6_DA1_AC1_ID_86_saturation__End.m  | grep 100000.0 | awk '{print $6}'`
thrp6=`tail -n 2 thrp_SA7_DA1_AC1_ID_100_saturation__End.m | grep 100000.0 | awk '{print $6}'`
echo PFA, StarFighter, $thrp1, $thrp2, $thrp3, $thrp4, $thrp5, $thrp6
cd ..

cd PFA_Vincent
thrp1=`tail -n 2 thrp_SA2_DA1_AC1_ID_30_saturation__End.m  | grep 100000.0 | awk '{print $6}'`
thrp2=`tail -n 2 thrp_SA3_DA1_AC1_ID_44_saturation__End.m  | grep 100000.0 | awk '{print $6}'`
thrp3=`tail -n 2 thrp_SA4_DA1_AC1_ID_58_saturation__End.m  | grep 100000.0 | awk '{print $6}'`
thrp4=`tail -n 2 thrp_SA5_DA1_AC1_ID_72_saturation__End.m  | grep 100000.0 | awk '{print $6}'`
thrp5=`tail -n 2 thrp_SA6_DA1_AC1_ID_86_saturation__End.m  | grep 100000.0 | awk '{print $6}'`
thrp6=`tail -n 2 thrp_SA7_DA1_AC1_ID_100_saturation__End.m | grep 100000.0 | awk '{print $6}'`
echo PFA, Vincent, $thrp1, $thrp2, $thrp3, $thrp4, $thrp5, $thrp6
cd ..

cd PFA_Yacmoca
thrp1=`tail -n 2 thrp_SA2_DA1_AC1_ID_30_saturation__End.m  | grep 100000.0 | awk '{print $6}'`
thrp2=`tail -n 2 thrp_SA3_DA1_AC1_ID_44_saturation__End.m  | grep 100000.0 | awk '{print $6}'`
thrp3=`tail -n 2 thrp_SA4_DA1_AC1_ID_58_saturation__End.m  | grep 100000.0 | awk '{print $6}'`
thrp4=`tail -n 2 thrp_SA5_DA1_AC1_ID_72_saturation__End.m  | grep 100000.0 | awk '{print $6}'`
thrp5=`tail -n 2 thrp_SA6_DA1_AC1_ID_86_saturation__End.m  | grep 100000.0 | awk '{print $6}'`
thrp6=`tail -n 2 thrp_SA7_DA1_AC1_ID_100_saturation__End.m | grep 100000.0 | awk '{print $6}'`
echo PFA, Yacmoca, $thrp1, $thrp2, $thrp3, $thrp4, $thrp5, $thrp6
cd ..

cd Sneaky_Sneaky
thrp1=`tail -n 2 thrp_SA2_DA1_AC1_ID_30_saturation__End.m  | grep 100000.0 | awk '{print $6}'`
thrp2=`tail -n 2 thrp_SA3_DA1_AC1_ID_44_saturation__End.m  | grep 100000.0 | awk '{print $6}'`
thrp3=`tail -n 2 thrp_SA4_DA1_AC1_ID_58_saturation__End.m  | grep 100000.0 | awk '{print $6}'`
thrp4=`tail -n 2 thrp_SA5_DA1_AC1_ID_72_saturation__End.m  | grep 100000.0 | awk '{print $6}'`
thrp5=`tail -n 2 thrp_SA6_DA1_AC1_ID_86_saturation__End.m  | grep 100000.0 | awk '{print $6}'`
thrp6=`tail -n 2 thrp_SA7_DA1_AC1_ID_100_saturation__End.m | grep 100000.0 | awk '{print $6}'`
echo Sneaky, Sneaky, $thrp1, $thrp2, $thrp3, $thrp4, $thrp5, $thrp6
cd ..

cd Sneaky_StarFighter
thrp1=`tail -n 2 thrp_SA2_DA1_AC1_ID_30_saturation__End.m  | grep 100000.0 | awk '{print $6}'`
thrp2=`tail -n 2 thrp_SA3_DA1_AC1_ID_44_saturation__End.m  | grep 100000.0 | awk '{print $6}'`
thrp3=`tail -n 2 thrp_SA4_DA1_AC1_ID_58_saturation__End.m  | grep 100000.0 | awk '{print $6}'`
thrp4=`tail -n 2 thrp_SA5_DA1_AC1_ID_72_saturation__End.m  | grep 100000.0 | awk '{print $6}'`
thrp5=`tail -n 2 thrp_SA6_DA1_AC1_ID_86_saturation__End.m  | grep 100000.0 | awk '{print $6}'`
thrp6=`tail -n 2 thrp_SA7_DA1_AC1_ID_100_saturation__End.m | grep 100000.0 | awk '{print $6}'`
echo Sneaky, StarFighter, $thrp1, $thrp2, $thrp3, $thrp4, $thrp5, $thrp6
cd ..

cd Sneaky_Vincent
thrp1=`tail -n 2 thrp_SA2_DA1_AC1_ID_30_saturation__End.m  | grep 100000.0 | awk '{print $6}'`
thrp2=`tail -n 2 thrp_SA3_DA1_AC1_ID_44_saturation__End.m  | grep 100000.0 | awk '{print $6}'`
thrp3=`tail -n 2 thrp_SA4_DA1_AC1_ID_58_saturation__End.m  | grep 100000.0 | awk '{print $6}'`
thrp4=`tail -n 2 thrp_SA5_DA1_AC1_ID_72_saturation__End.m  | grep 100000.0 | awk '{print $6}'`
thrp5=`tail -n 2 thrp_SA6_DA1_AC1_ID_86_saturation__End.m  | grep 100000.0 | awk '{print $6}'`
thrp6=`tail -n 2 thrp_SA7_DA1_AC1_ID_100_saturation__End.m | grep 100000.0 | awk '{print $6}'`
echo Sneaky, Vincent, $thrp1, $thrp2, $thrp3, $thrp4, $thrp5, $thrp6
cd ..

cd Sneaky_Yacmoca
thrp1=`tail -n 2 thrp_SA2_DA1_AC1_ID_30_saturation__End.m  | grep 100000.0 | awk '{print $6}'`
thrp2=`tail -n 2 thrp_SA3_DA1_AC1_ID_44_saturation__End.m  | grep 100000.0 | awk '{print $6}'`
thrp3=`tail -n 2 thrp_SA4_DA1_AC1_ID_58_saturation__End.m  | grep 100000.0 | awk '{print $6}'`
thrp4=`tail -n 2 thrp_SA5_DA1_AC1_ID_72_saturation__End.m  | grep 100000.0 | awk '{print $6}'`
thrp5=`tail -n 2 thrp_SA6_DA1_AC1_ID_86_saturation__End.m  | grep 100000.0 | awk '{print $6}'`
thrp6=`tail -n 2 thrp_SA7_DA1_AC1_ID_100_saturation__End.m | grep 100000.0 | awk '{print $6}'`
echo Sneaky, Yacmoca, $thrp1, $thrp2, $thrp3, $thrp4, $thrp5, $thrp6
cd ..

cd StarFighter_StarFighter
thrp1=`tail -n 2 thrp_SA2_DA1_AC1_ID_30_saturation__End.m  | grep 100000.0 | awk '{print $6}'`
thrp2=`tail -n 2 thrp_SA3_DA1_AC1_ID_44_saturation__End.m  | grep 100000.0 | awk '{print $6}'`
thrp3=`tail -n 2 thrp_SA4_DA1_AC1_ID_58_saturation__End.m  | grep 100000.0 | awk '{print $6}'`
thrp4=`tail -n 2 thrp_SA5_DA1_AC1_ID_72_saturation__End.m  | grep 100000.0 | awk '{print $6}'`
thrp5=`tail -n 2 thrp_SA6_DA1_AC1_ID_86_saturation__End.m  | grep 100000.0 | awk '{print $6}'`
thrp6=`tail -n 2 thrp_SA7_DA1_AC1_ID_100_saturation__End.m | grep 100000.0 | awk '{print $6}'`
echo StarFighter, StarFighter, $thrp1, $thrp2, $thrp3, $thrp4, $thrp5, $thrp6
cd ..

cd StarFighter_Vincent
thrp1=`tail -n 2 thrp_SA2_DA1_AC1_ID_30_saturation__End.m  | grep 100000.0 | awk '{print $6}'`
thrp2=`tail -n 2 thrp_SA3_DA1_AC1_ID_44_saturation__End.m  | grep 100000.0 | awk '{print $6}'`
thrp3=`tail -n 2 thrp_SA4_DA1_AC1_ID_58_saturation__End.m  | grep 100000.0 | awk '{print $6}'`
thrp4=`tail -n 2 thrp_SA5_DA1_AC1_ID_72_saturation__End.m  | grep 100000.0 | awk '{print $6}'`
thrp5=`tail -n 2 thrp_SA6_DA1_AC1_ID_86_saturation__End.m  | grep 100000.0 | awk '{print $6}'`
thrp6=`tail -n 2 thrp_SA7_DA1_AC1_ID_100_saturation__End.m | grep 100000.0 | awk '{print $6}'`
echo StarFighter, Vincent, $thrp1, $thrp2, $thrp3, $thrp4, $thrp5, $thrp6
cd ..

cd StarFighter_Yacmoca
thrp1=`tail -n 2 thrp_SA2_DA1_AC1_ID_30_saturation__End.m  | grep 100000.0 | awk '{print $6}'`
thrp2=`tail -n 2 thrp_SA3_DA1_AC1_ID_44_saturation__End.m  | grep 100000.0 | awk '{print $6}'`
thrp3=`tail -n 2 thrp_SA4_DA1_AC1_ID_58_saturation__End.m  | grep 100000.0 | awk '{print $6}'`
thrp4=`tail -n 2 thrp_SA5_DA1_AC1_ID_72_saturation__End.m  | grep 100000.0 | awk '{print $6}'`
thrp5=`tail -n 2 thrp_SA6_DA1_AC1_ID_86_saturation__End.m  | grep 100000.0 | awk '{print $6}'`
thrp6=`tail -n 2 thrp_SA7_DA1_AC1_ID_100_saturation__End.m | grep 100000.0 | awk '{print $6}'`
echo StarFighter, Yacmoca, $thrp1, $thrp2, $thrp3, $thrp4, $thrp5, $thrp6
cd ..

cd Vincent_Vincent
thrp1=`tail -n 2 thrp_SA2_DA1_AC1_ID_30_saturation__End.m  | grep 100000.0 | awk '{print $6}'`
thrp2=`tail -n 2 thrp_SA3_DA1_AC1_ID_44_saturation__End.m  | grep 100000.0 | awk '{print $6}'`
thrp3=`tail -n 2 thrp_SA4_DA1_AC1_ID_58_saturation__End.m  | grep 100000.0 | awk '{print $6}'`
thrp4=`tail -n 2 thrp_SA5_DA1_AC1_ID_72_saturation__End.m  | grep 100000.0 | awk '{print $6}'`
thrp5=`tail -n 2 thrp_SA6_DA1_AC1_ID_86_saturation__End.m  | grep 100000.0 | awk '{print $6}'`
thrp6=`tail -n 2 thrp_SA7_DA1_AC1_ID_100_saturation__End.m | grep 100000.0 | awk '{print $6}'`
echo Vincent, Vincent, $thrp1, $thrp2, $thrp3, $thrp4, $thrp5, $thrp6
cd ..

cd Vincent_Yacmoca
thrp1=`tail -n 2 thrp_SA2_DA1_AC1_ID_30_saturation__End.m  | grep 100000.0 | awk '{print $6}'`
thrp2=`tail -n 2 thrp_SA3_DA1_AC1_ID_44_saturation__End.m  | grep 100000.0 | awk '{print $6}'`
thrp3=`tail -n 2 thrp_SA4_DA1_AC1_ID_58_saturation__End.m  | grep 100000.0 | awk '{print $6}'`
thrp4=`tail -n 2 thrp_SA5_DA1_AC1_ID_72_saturation__End.m  | grep 100000.0 | awk '{print $6}'`
thrp5=`tail -n 2 thrp_SA6_DA1_AC1_ID_86_saturation__End.m  | grep 100000.0 | awk '{print $6}'`
thrp6=`tail -n 2 thrp_SA7_DA1_AC1_ID_100_saturation__End.m | grep 100000.0 | awk '{print $6}'`
echo Vincent, Yacmoca, $thrp1, $thrp2, $thrp3, $thrp4, $thrp5, $thrp6
cd ..

cd Yacmoca_Yacmoca
thrp1=`tail -n 2 thrp_SA2_DA1_AC1_ID_30_saturation__End.m  | grep 100000.0 | awk '{print $6}'`
thrp2=`tail -n 2 thrp_SA3_DA1_AC1_ID_44_saturation__End.m  | grep 100000.0 | awk '{print $6}'`
thrp3=`tail -n 2 thrp_SA4_DA1_AC1_ID_58_saturation__End.m  | grep 100000.0 | awk '{print $6}'`
thrp4=`tail -n 2 thrp_SA5_DA1_AC1_ID_72_saturation__End.m  | grep 100000.0 | awk '{print $6}'`
thrp5=`tail -n 2 thrp_SA6_DA1_AC1_ID_86_saturation__End.m  | grep 100000.0 | awk '{print $6}'`
thrp6=`tail -n 2 thrp_SA7_DA1_AC1_ID_100_saturation__End.m | grep 100000.0 | awk '{print $6}'`
echo Yacmoca, Yacmoca, $thrp1, $thrp2, $thrp3, $thrp4, $thrp5, $thrp6
cd ..
