package layer2_802Algorithms;

import layer2_80211Mac.JE802_11BackoffEntity;
import layer2_80211Mac.JE802_11Mac;
import layer2_80211Mac.JE802_11MacAlgorithm;

public class throughput extends JE802_11MacAlgorithm {

	private JE802_11BackoffEntity theBackoffEntity;

	public throughput(String name, JE802_11Mac mac) {
		super(name, mac);
		this.theBackoffEntity = this.mac.getBackoffEntity(1);
	}
	
	@Override
	public void compute() {
		message("--- Discarded packets: " + this.theBackoffEntity.getTheDiscardCnt());
		message("    Max num of slots in MAC queue: " + this.theBackoffEntity.getQueueSize() + ", out of which currently used: " + this.theBackoffEntity.getCurrentQueueSize());
		message("    Num of packet transmission attempts: " + this.theBackoffEntity.getTheTxCnt());
		message("    Num of bytes transmission attempts: " + this.theBackoffEntity.getTheTxByteCnt());
		message("    Num of received acknowledgments: " + this.theBackoffEntity.getTheAckCnt());
		message("    Estimated num of collisions: " + this.theBackoffEntity.getTheCollisionCnt());
	}
	
	@Override
	public void plot() {
		// TODO Auto-generated method stub
		
	}

}
