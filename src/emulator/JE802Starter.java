/*
 * 
 * This is Jemula.
 *
 *    Copyright (c) 2009 Stefan Mangold, Fabian Dreier, Stefan Schmid
 *    All rights reserved. Urheberrechtlich geschuetzt.
 *    
 *    Redistribution and use in source and binary forms, with or without modification,
 *    are permitted provided that the following conditions are met:
 *    
 *      Redistributions of source code must retain the above copyright notice,
 *      this list of conditions and the following disclaimer. 
 *    
 *      Redistributions in binary form must reproduce the above copyright notice,
 *      this list of conditions and the following disclaimer in the documentation and/or
 *      other materials provided with the distribution. 
 *    
 *      Neither the name of any affiliation of Stefan Mangold nor the names of its contributors
 *      may be used to endorse or promote products derived from this software without
 *      specific prior written permission. 
 *    
 *    THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY
 *    EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
 *    OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 *    IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 *    INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 *    BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA,
 *    OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 *    WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 *    ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY
 *    OF SUCH DAMAGE.
 *    
 */

package emulator;

import java.io.File;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.w3c.dom.Document;

import static java.nio.file.StandardCopyOption.REPLACE_EXISTING;

public class JE802Starter {

  /**
   * @param args
   */
  public static void main(String[] args) {
    if (args.length != 1) {
      System.err.println("This is Jemula802. Please provide path and name of the XML scenario file.");
      return;
    }

    String aScenarioFile = args[0];
    Document configuration = parseDocument(aScenarioFile);
    JE802Control control = new JE802Control(configuration);

    // copy scenario XML file into target directory, for archiving and/or later analysis
    copyFile(aScenarioFile, control.getPath2Results());

    control.emulate(); // run the show (by starting scheduler)
    control.visualize(); // create google earth animation
  }

  private static Document parseDocument(String aScenarioFilename) {
    Document anXMLdoc = null;
    File theScenarioFile = new File(aScenarioFilename); // The file to parse
    if (!theScenarioFile.exists()) { // the XML scenario file does not exist or is not accessible
      System.err.println("This is Jemula802. Error: could not open the XML scenario description file "
              + theScenarioFile);
      System.exit(0);
    }

    System.out.println(" JE802Starter: This is Jemula802.\n JE802Starter: XML scenario file is \""
            + theScenarioFile.getName() + "\"");
    DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();

    try {
      DocumentBuilder parser = factory.newDocumentBuilder();
      anXMLdoc = parser.parse(theScenarioFile);
    } catch (Exception e) {
      e.printStackTrace();
    }

    return anXMLdoc;
  }

  public static void copyFile(String sourceFile, String destDirectory) {
    Path source = Paths.get(sourceFile);
    Path target = Paths.get(destDirectory + File.separator + source.getFileName());

    try {
      Files.createDirectories(Paths.get(destDirectory));
      Files.copy(source, target, REPLACE_EXISTING);
    } catch (Exception e) {
      e.printStackTrace();
    }
  }
}
